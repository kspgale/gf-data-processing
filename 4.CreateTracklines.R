#CreateTracklines.R
## Create Tracklines of Fishing Events

library(sf)

rm(list=ls())

# CRS codes
geogr<-"EPSG:4269" # NAD 83
bcalb<-"EPSG:3005" # BC Albers




setwd("D:/Documents/Data files/Data Extractions and Sharing/Groundfish")
inFile<-c("./4.SiteXSpeciesMatrix/GFBio_HBLL_2021-04-01_cwcs_siteXtaxon.txt")
outLoc<-c("./5.Shapefiles")
outFile<-c("HBLL_2021-04-01_cwcs_FExspecies") #date is date of pull not today's date
logFile<-c("./5.Shapefiles/Synoptic_2021-04-01_cwcs_FExspecies.Rhistory")

## Trim lines by a given length? set in km
trim<-10

#Load data
dat<-read.csv(inFile)
toIgnore<-c("FISHING_EV","LatStart","LonStart","LatEnd","LonEnd","year","month","Year","Month")
origNames<-data.frame(origName=names(dat)[!names(dat)%in% toIgnore])

names(dat)[names(dat)=="Sebastes.melanostomus"]<-"Sebastes.mstomus"
names(dat)[names(dat)=="Sebastes.aleutianus.melanostictus"]<-"Sebastes.aleutianus"

dat2<-dat
names(dat2)[!names(dat2)%in% toIgnore]<-  paste(substr(names(dat2)[!names(dat2)%in% toIgnore], 1,1),substr(gsub((".*\\."), "", names(dat2)[!names(dat2)%in% toIgnore]), 1,8),sep=".")
names(dat2)[names(dat)=="Apostichopus.californicus"]<-"Ap.californicus"
names(dat2)[names(dat)=="Armina.californica"]<-"Ar.califor"
names(dat2)[names(dat)=="Cancer.productus"]<-"Ca.product"
names(dat2)[names(dat)=="Chelyosoma.productum"]<-"Ch.product"
names(dat2)[names(dat)=="Leptychaster.pacificus"]<-"Le.pacific"
names(dat2)[names(dat)=="Lycodes.pacificus"]<-"Ly.pacific"
names(dat2)[names(dat)=="Paralithodes.camtschaticus"]<-"Pa.camtsch"
names(dat2)[names(dat)=="Pasiphaea.pacifica"]<-"Pa.pacific"
names(dat2)[names(dat)=="Phacellophora.camtschatica"]<-"Ph.camtsch"
names(dat2)[names(dat)=="Primnoa.pacifica"]<-"Pr.pacific"

#check that nrow=nFE
nrow(dat2)==length(unique(dat$FISHING_EVENT_ID))

# remove NA coordinates
dat2<-dat2[!is.na(dat2$LatStart),]

#Get LINES shapefile
# ranlines <- list()
# for (irow in 1:nrow(dat2)) {
#   ranlines[[irow]] <- Lines(Line(rbind(as.numeric(dat[irow, c("LonStart",
#                                                                 "LatStart")]), as.numeric(dat2[irow, c("LonEnd", "LatEnd")]))),
#                             ID = as.character(irow))
# }
# sldf <- SpatialLinesDataFrame(SpatialLines(ranlines), dat2, match.ID = FALSE)

dat_line1<-dat2[,c("F.FISHING_","LatStart","LonStart")] 
dat_line2<-dat2[,c("F.FISHING_","LatEnd","LonEnd")]
names(dat_line1)<-c("FE","Lat","Lon")
names(dat_line2)<-c("FE","Lat","Lon")
# dat_line1$seq<-1
# dat_line2$seq<-2

# Create list of simple feature geometries (linestrings)
l_sf <- vector("list", nrow(dat_line1))
for (i in seq_along(l_sf)){
  l_sf[[i]] <- st_linestring(as.matrix(rbind(dat_line1[i, c(3,2)], dat_line2[i,c(3,2)])))
}
# Create simple feature geometry list column
l_sfc <- st_sfc(l_sf, crs = geogr)
sldf<-st_sf(dat2,geometry=l_sfc)

sldf <- st_transform(sldf, bcalb) #project


# remove lines over designated length
 # sldf@data$length<-gLength(sldf, byid=TRUE)
 # hist(sldf@data$length)
 # sldf<-subset(sldf, length<=trim*1000)
 
sldf$length<-st_length(sldf)
hist(sldf$length)
sldf<-sldf[as.numeric(sldf$length)<= trim*1000,]

#WriteShapefile 
names(sldf) <- strtrim(names(sldf),10)
origNames$shortName<-names(sldf)[!names(sldf)%in% c(toIgnore, "geometry","length")]
write.csv(origNames,paste0(outLoc,"/",outFile, "_names.csv"), row.names=F)

st_write(sldf, dsn=outLoc, layer=outFile, driver="ESRI Shapefile", overwrite=T)

savehistory(logFile)
