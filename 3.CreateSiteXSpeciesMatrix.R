#CreateSiteXSpeciesMatrix.R
#Take any file with a fishing event and species and turn it into a site x species matrix

library(reshape)
library(plyr)
library(dplyr)
library(vegan)

############################
## Set up files and limits
############################

rm(list=ls())

setwd("D:/Documents/Data files/Data Extractions and Sharing/Groundfish")
inFile<-c("./3.ProcessedLongMode/GFBio_HBLL-Surveys_HookCountSet_2021-04-01_processed.txt")
outFile<-c("./4.SiteXSpeciesMatrix/GFBio_HBLL_2021-04-01") #Date is date of pull, not today's date
logFile<-c("./4.SiteXSpeciesMatrix/GFBio_HBLL_2021-04-01")

#CPUE or PA etc (name of value field)
unit<-"CtPerHookPerHr"

#Limit by...
speciesOnly<-"no"
fishOnly<-"no"
invertsOnly<-"no"
spongeCoralOnly<-"yes"

##############
###Run Code
##############
dat<-read.csv(inFile)
head(dat)


#Get Info For Each FE
FEInfo<-unique(dat[,names(dat) %in% c("FISHING_EVENT_ID","LatStart", "LonStart","LatEnd","LonEnd", "year","month", "Month","Year")])
head(FEInfo)

if(speciesOnly=="yes"){
dat<-dat[dat$TaxonLevel=="species",]
} else {}

if(fishOnly=="yes"){
dat<-dat[dat$Class %in% c("Actinopteri","Elasmobranchii","Myxini","Holocephali","Petromyzonti")|
         dat$ValidName %in% c("Gnathostomata","Pisces"),]  
} else {}

if(invertsOnly=="yes"){
dat<-dat[dat$Class %in% c("Ascidiacea","Thaliacea")|
         dat$Phylum %in% c("Porifera","Echinodermata","Arthropoda","Cnidaria","Mollusca", "Annelida","Brachiopoda","Bryozoa","Ctenophora","Nemertea","Platyhelminthes", "Sipuncula")|
         dat$ValidName=="Tunicata",]
} else {}

if(spongeCoralOnly=="yes"){
  cwcs<-read.csv("D:/Documents/Projects/SpongeCoralHotspots_GroundfishTrawl/2020_TrawlUtilization_Hotspots/1_InputData/speciescodes/AllCoralSponge_HartCodes_Groups_kg.csv")
  dat<-merge(dat, cwcs, by="SPECIES_CODE")
} else {}


head(dat)
#Melt, Cast to get FE x Taxa
dat2<-dat[,c("FISHING_EVENT_ID","ValidName",unit)]
datMelt<-melt(dat2, id.vars=c("FISHING_EVENT_ID","ValidName"))
datCast<-cast(datMelt, FISHING_EVENT_ID~ValidName, fun=sum) #site x species
dim(datCast) #412 x 73 taxa +1 col

datCastJoin<-join(datCast, FEInfo, by="FISHING_EVENT_ID") #attach locations
dim(datCastJoin) #412 x 74 taxa +6 col
head(datCastJoin)

write.csv(datCastJoin, paste0(outFile,ifelse(fishOnly=="yes","_fish",ifelse(invertsOnly=="yes", "_inverts",ifelse(spongeCoralOnly=="yes", "_cwcs",""))), ifelse(speciesOnly=="yes","_species",""), "_siteXtaxon.txt") , row.names=F)
savehistory(paste0(logFile, ifelse(fishOnly=="yes","_fish",ifelse(invertsOnly=="yes", "_inverts",ifelse(spongeCoralOnly=="yes", "_cwcs",""))), ifelse(speciesOnly=="yes","_species",""), "_siteXtaxon.Rhistory"))
