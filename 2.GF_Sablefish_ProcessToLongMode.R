# GF_Sablefish_ProcessToLongMode.R
#1. Query out of GFBio
#2. Run through fixNames.R
#3. Run Processing to get to long Mode (many of the steps in this code could be accomplished through a more thoughtful SQL query)

rm(list=ls())

library(data.table)
library(plyr)
library(reshape)
library(vegan)
library(dplyr)
library(lubridate)
library(sp)
library(rgdal)

setwd("D:/Documents/Data files/Data Extractions and Sharing/Groundfish")
inFile<-c("./2.NamesCleaned/GFBio_SablefishRandom_withTraps_2019-12-06allTaxa.txt")
outFile<-c("./3.ProcessedLongMode/GFBio_SablefishRandom_withTraps_2019-12-06_Processed.txt")
logFile<-c("./3.ProcessedLongMode/GFBio_SablefishRandom_withTraps_2019-12-06_ProcessingLog.Rhistory")


#Import table
sablefish<-data.table(read.csv(inFile))

print(c("geartype is", paste(unique(sablefish$GEAR_DESC)))) #all trap

#Add unique ID number to keep track of individual records, and check if there are more than 1 record for each species in each FE
sablefish$FE_SP<-paste(sablefish$FISHING_EVENT_ID, sablefish$SPECIES_CODE,sep="-")
cat(length(unique(sablefish$FE_SP)), "unique IDs\n")#2318 IDs
cat(nrow(sablefish), "rows") #2388 - so there are about 70 records of species weighed twice per tow

#calculate Coordinates
sablefish$LatStart<-(sablefish$FE_START_LATTITUDE_DEGREE+(sablefish$FE_START_LATTITUDE_MINUTE/60))
sablefish$LatEnd<-(sablefish$FE_END_LATTITUDE_DEGREE+(sablefish$FE_END_LATTITUDE_MINUTE/60))
sablefish$LonStart<-(-1)*(sablefish$FE_START_LONGITUDE_DEGREE+(sablefish$FE_START_LONGITUDE_MINUTE/60))
sablefish$LonEnd<-(-1)*(sablefish$FE_END_LONGITUDE_DEGREE+(sablefish$FE_END_LONGITUDE_MINUTE/60))

#change start and end fishing dates into proper format
#To get duration, times must be in "POSIXct" format
sablefish$start.fishing<-as.POSIXct(sablefish$FE_END_DEPLOYMENT_TIME,format ="%Y-%m-%d %H:%M:%S")
sablefish$end.fishing<-as.POSIXct(sablefish$FE_BEGIN_RETRIEVAL_TIME,format ="%Y-%m-%d %H:%M:%S")

#Calculate duration of tow in seconds
sablefish$fishing.duration.s<-sablefish$end.fishing-sablefish$start.fishing; units(sablefish$fishing.duration.s)<-"secs"
str(sablefish$fishing.duration.s) #time in seconds
sablefish$fishing.duration.s<-as.numeric(sablefish$fishing.duration.s)
summary(sablefish$fishing.duration.s) #61740-317100 seconds

#add month and year columns
sablefish$month<-month(sablefish$start.fishing)
sablefish$year<-year(sablefish$start.fishing)

#Drop those without fishing durations (meaning either start or end fishing times were not available)
sablefish2<-sablefish[!is.na(sablefish$fishing.duration.s),]
nrow(sablefish2) #2388 species records

#start to calculate CPUE - depending on tow and species, some records are count and some are weight
#Here, count is the more appropriate metric
summary(sablefish2$CATCH_WEIGHT) #280 NAs - most of these records are not weighed!
summary(sablefish2$CATCH_COUNT) #112 NAs

#sum weights and counts for each species-tow (some species were counted more than once per tow)
summarybytow<-sablefish2[,list(total_wt=sum(CATCH_WEIGHT, na.rm=T), total_count=sum(CATCH_COUNT, na.rm=T)), by=FE_SP]
nrow(summarybytow) #2318 species records
length(unique(summarybytow$FE_SP)) #2318 species-tow IDs

#Add the summary to the dataframe
#we've now grouped species records within each fishing event. Since there were multiple species records for some FEs, there may be duplicates when we merge
#Remove duplicates (doesn't matter which one)
sablefish3<-merge(sablefish2, summarybytow, by="FE_SP") #2388
sablefish3<-subset(sablefish3, !duplicated(FE_SP)) # 2318
head(sablefish3)

#Look at catch values to see which records should be retained
#Some things are not counted and are not weighed. These may be errors, or might be because they are "trace" records
#If the verification code/description doesn't say trace, and indicates that it was counted or weighed, double zero is probably an error and should be dropped
bothNA<-subset(sablefish3, (is.na(total_wt) & is.na(total_count))) #0 obs
bothZero<-subset(sablefish3, (total_wt==0 & total_count==0)) #check weights and counts that are both zero - 89 records
head(bothZero)
bothZero$SPECIES_COMMON_NAME<-droplevels(bothZero$SPECIES_COMMON_NAME)
summary(bothZero$SPECIES_COMMON_NAME) #inverts or blank
bothZero[SPECIES_COMMON_NAME!="UNKNOWN FISH",] #do not have verification codes so can't assess - just drop.
sablefish4<-sablefish3[!(sablefish3$FE_SP %in% bothZero$FE_SP),] #2229

#Calculate CPUE
#For longline, we're using count per trap per hour (CtPerTrapPerHr)
head(sablefish4)
range(sablefish4$TRAPS_FISHED_COUNT) #21-27 "The number of traps fished during a fishing  event"
sablefish4$CtPerTrapPerHr<-sablefish4$total_count/sablefish4$TRAPS_FISHED_COUNT/(sablefish4$fishing.duration.s/3600)

#CPUE can be inflated by short fishing durations
#drop records with fishing duration < 180 (3 minutes)
sablefish4<-sablefish4[sablefish4$fishing.duration.s> 180,] #2229
names(sablefish4)

sablefishDat<-sablefish4[, c("FE_SP","TRIP_ID","FISHING_EVENT_ID","GEAR_DESC","year", "month",
                                    "LatStart","LonStart","LatEnd","LonEnd","start.fishing","end.fishing","fishing.duration.s","TRAPS_FISHED_COUNT",
                                    "SPECIES_CODE","ValidName","Phylum","Order","Class","TaxonLevel","total_wt","total_count","CtPerTrapPerHr" ), with=F]
head(sablefishDat)

write.csv(sablefishDat, outFile,row.names=F)

savehistory(logFile)
