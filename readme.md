# Processing Scripts for Groundfish Biological (GFBio) Data
Author: Katie Gale (katie.gale@dfo-mpo.gc.ca)

Readme date: 6 April 2018

Description: Standard scripts for moving from GFBio outputs into usable dataframes, site x species matrices, and shapefiles for inputs into other analyses.



#### 1.FixNames.R
Take the output from GFBio and add in current taxonomic names and higher level taxonomy using a lookup table.

#### 2.GF_PHMA/Sablefish/Synoptic_ProcessToLongMode.R
Take the name-corrected file and process it into a dataframe ready for analysis, with one species record per row.
- sums multiple catch records per location into a single record
- calculates CPUE (PHMA = count/hook/hr; sablefish = count/hook/hr; synoptic = weight/hr)
- removes fishing events with duration < 3 minutes
- writes a processing log

#### 3.CreateSiteXSpeciesMatrix
Create a matrix from a "long-mode" data table of species records.
- Can specify if you want species-level taxa only, fish only, or inverts only.
- writes a processing log

#### CalculateDiversityRichnessBiomass.R
Take a site x species matrix and calculate diversity metrics for each site (row).
- note that diversity and richness metrics can take species-level taxa only. Total CPUE per site can take any/all taxa.
- writes a processing log

#### CreateTracklines.R
Take a site x species matrix with start and end coordinates and create a line shapefile
- truncates species-genus names into max 10 character strings in the format `s.genus` (e.g., `H.oregonen`, `H.nudus`). Code will make known ambiguous names unique. e.g., Cancer productus = `Ca.product`, Chelysoma productum = `Ch.product`.
- includes parameter to trim lines into reasonable lengths. Default is 10 km.
