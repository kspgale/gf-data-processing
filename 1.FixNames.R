# FixNames.R
# Katie Gale
# 4 August 2017

#For any dataframe, cross-reference with WoRMS lookup table and add up-to-date name and taxonomic level
#Report if any names did not match
#Report record numbers to ensure good joins
#But, does not check if there are duplicate records per species per fishing event. 

library(plyr)
library(data.table)

#### ----- ###
## INPUT INFO HERE
#Set wd
setwd("G:/MyDocumentsMirror/")

#Bring in table of interest
  dat<-read.csv("Data files/Data Extractions and Sharing/Groundfish/1.InitialQuery/GFBio_HBLL-Surveys_HookCountSet_2021-04-01.txt")
# unique(dat$SpeciesID)

# #For WCTSS Dive only
# dat$Hart_Code<-as.character(dat$Hart_Code)
# dat$Hart_Code[dat$Hart_Code=="4JG, 4JE"]<-"4JN"
# dat$Hart_Code[dat$Hart_Code=="P23, P24"]<-"PNX"
# dat$Hart_Code[dat$Hart_Code=="76E, 76F"]<-"76D"
# dat$Hart_Code[dat$Hart_Code=="66D, 66E"]<-"66T"
# dat$Hart_Code[dat$Hart_Code=="98H, 98G"]<-"98D"
# dat$Hart_Code[dat$Hart_Code=="3N4, 3N3"]<-"3N2"
# dat$Hart_Code[dat$Hart_Code=="4TE, 4TC, "]<-"4TO"
# dat$Hart_Code[dat$Hart_Code=="67C, 67E"]<-"66Z"
# dat$Hart_Code[dat$Hart_Code=="8FW, "]<-"8FW"
# dat$Hart_Code[dat$Hart_Code=="3JK, 3J7"]<-"3J6"
# dat$Hart_Code[dat$Hart_Code=="VLH, VLI"]<-"VLG"
# dat$Hart_Code[dat$Hart_Code=="82R"]<-"82E"
# dat$Hart_Code[dat$Hart_Code=="8VB"]<-"8BV"
# dat$Hart_Code[dat$Hart_Code=="8YE"]<-"8EY"

# dat<-allDat
head(dat)
nrowdat<-nrow(dat)

#Set science name column
#If no science name given, set to ""
origScienceName<-c("SPECIES_SCIENCE_NAME")
speciesCode<-c("SPECIES_CODE")

#Output Path
writeCSV<-"yes"
out<-c("Data files/Data Extractions and Sharing/Groundfish/2.NamesCleaned/GFBio_HBLL-Surveys_HookCountSet_2021-04-01")


######## ---------------###########
## Load reference tables
#If no science names are in the original file, bring in the hart codes
if(origScienceName=="") {
  #Bring in Hart Codes
  hart<-read.csv("./Data files/Databases_Shellfish/Specieshrt.csv")
  hart<-hart[,c(1,2)]
  names(hart)<-c(speciesCode, "OriginalName")
  
  c<-match(speciesCode,names(dat))
  dat[,c]<-toupper(dat[,c])
  dat<-join(dat, hart, by=speciesCode)
  origScienceName<-"OriginalName"
  head(dat)
} else {
  c<-match(speciesCode,names(dat))
  dat[,c]<-toupper(dat[,c])
}
head(dat)
dat[dat$Habitat.Species=="114",]

#Bring in SpeciesNames from WoRMS 
specNames<-read.delim("./Data files/Taxonomy/Taxonomy.txt",sep="\t")
specNames<-specNames[,names(specNames) %in% c("ScientificName","ScientificName_accepted","Phylum","Class","Order")]
specNames<-unique(specNames) 
names(specNames)[1:2]<-c(origScienceName,"ValidName")

#Bring in file that has taxon level for each name
taxonLevel<-read.csv("./Data files/Taxonomy//Species_TaxonLevel.csv")
names(taxonLevel)<-c("ValidName","TaxonLevel")
taxonLevel<-unique(taxonLevel)

######## ---------------###########
## Start clean

#Make both original and WoRMS lookup names lowercase to match
specNames[,(match(origScienceName,names(specNames)))]<-tolower(specNames[,(match(origScienceName,names(specNames)))])
b<-match(origScienceName,names(dat))
dat[,b]<-tolower(dat[,b])

#change blackspotted to rougheye to make rougheye-blackspotted 
dat[,b][dat[,c]==425]<-"sebastes aleutianus"
dat[,b][dat[,c]==394]<-"sebastes aleutianus"

#join corrected names
dat2<-join(dat, specNames, by=origScienceName, type="left")
# rm(dat)

# #remove NA records
dat3<-(dat2[!is.na(dat2$ValidName),])
dat3<-subset(dat3, ValidName!="")

dat3<-dat3[,!(names(dat3) %in% c("OriginalName",origScienceName))] #Remove these 2 columns

#Add taxonlevel to the dataframe
dat4<-join(dat3,taxonLevel, by="ValidName", type="left" )
dat4$TaxonLevel<-tolower(dat4$TaxonLevel)

sp<-subset(dat4, TaxonLevel=="species")

#Write outputs
if(tolower(writeCSV)=="yes"){write.csv(dat4, paste0(out, "allTaxa", ".txt"), row.names=F)
  message(cat("\nWrote allTaxa .txt"))
  #write.csv(sp, paste0(out, "species", ".txt"))
  #message(cat("Wrote species-only .txt"))
} else {
  }

##Print report
#Look at NAs and blanks
d<-match(origScienceName,names(dat2))
e<-match(speciesCode,names(dat2))
badOrig<-unique(cbind.data.frame(dat2[d][is.na(dat2$ValidName),], dat2[e][is.na(dat2$ValidName),]))
badOrig$comb<-paste0("(",badOrig[,1],"-", badOrig[,2],")")
blankValid<-unique(cbind.data.frame(dat2[d][dat2$ValidName=="",], dat2[e][dat2$ValidName=="",]))
blankValid<-subset(blankValid, !is.na(blankValid[1]))
blankValid$comb<-paste0("(",blankValid[,1],"-", blankValid[,2],")")

if(nrow(badOrig)==0) {message(cat("All original names parsed - good."))
} else {
message(cat(paste(nrow(badOrig), "original names did not parse. \nThe original names that did not parse are:",paste(badOrig[,3], collapse="; "),
                  "\n",nrow(dat2[is.na(dat2$ValidName),]), "records have been dropped. Go back and revise original names if this is not wanted.")))
}

if(nrow(blankValid)==0) {message(cat("No blank valid names - good."))
} else {
  message(cat(paste(nrow(blankValid), "blank valid names. \nThe associated original names are:",paste(blankValid[,3], collapse="; "),
                    "\n",nrow(data.table(dat2[dat2$ValidName==""&!is.na(dat2$ValidName),])), "records have been dropped. Go back and revise original names if this is not wanted.")))
}

message(cat(paste(nrowdat), "original records,", paste(nrow(dat2)), "records after merge,", paste(nrow(dat4), "records total. \n", nrow(sp), "were at the species level.")))
        